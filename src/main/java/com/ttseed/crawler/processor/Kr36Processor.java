/**
 *
 *
 * Created by paladin 
 * Created at 2016年11月10日
 */
package com.ttseed.crawler.processor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.JsonPathSelector;

/**
 * 抓取36kr的新闻
 * 
 * 因为36kr的新闻是AJAX读取，前台渲染，方法和虎嗅的完全不同。
 * 
 * 
 * 
 * @author paladin
 *
 */
public class Kr36Processor implements PageProcessor {
	private Site site = Site.me();// .setRetryTimes(3).setSleepTime(100);
	// private static int MAX_CONTENT_LENGTH = 16000;

	// http://36kr.com/api/post?column_id=&b_id=&per_page=20&_=1478833085224
	// "http://angularjs\\.cn/api/article/latest.*"
	private static final String LIST_URL = "http://36kr\\.com/api/post\\?column_id=&b_id=.*";

	// http://36kr.com/api/post/5056190

	/*
	 * (non-Javadoc)
	 * 
	 * @see us.codecraft.webmagic.processor.PageProcessor#process(us.codecraft.
	 * webmagic.Page)
	 */
	@Override
	public void process(Page page) {
		// page.addTargetRequests(page.getHtml().links().regex(".*/p/\\d+\\.html").all());
		//
		// String id =
		// page.getUrl().toString().replaceAll("(http://36kr\\.com/p/)",
		// "").replaceAll("(\\.html)", "");
		// System.out.println("Url:"+page.getUrl()+" id:" + id);
		// page.putField("title",
		// page.getHtml().xpath("//div[@id='J_post_wrapper_"+id+"']//h1/text()"));
		// String content = page.getHtml().smartContent().toString();
		// page.putField("content", content.substring(0,
		// Math.min(MAX_CONTENT_LENGTH, content.length())));
		// page.putField("url", page.getUrl().toString());

		// if (content.isEmpty()) {
		// page.setSkip(true);
		// }

		// http://36kr.com/api/post?column_id=&b_id=&per_page=20&_=1478833085224

		// System.out.println(page.getUrl());
		if (page.getUrl().regex(LIST_URL).match()) {
			// System.out.println(page.getRawText());
			List<String> ids = new JsonPathSelector("$.data.items[*].id").selectList(page.getRawText());
			if (CollectionUtils.isNotEmpty(ids)) {
				for (String id : ids) {
					page.addTargetRequest("http://36kr.com/api/post/" + id + "?_=" + System.currentTimeMillis());
					// System.out.println(id);
					// System.currentTimeMillis()
					// new Date().getTime()
				}
				// TODO: 需要加入循环读取列表的控制代码
			}
		} else {
			page.putField("url",
					"http://36kr.com/p/" + new JsonPathSelector("$.data.id").select(page.getRawText()) + ".html");
			page.putField("title", new JsonPathSelector("$.data.title").select(page.getRawText()));
			// 数据为HTML unicode Entities格式[&#x690D;]，需要转换
			page.putField("content",
					StringEscapeUtils.unescapeHtml4(new JsonPathSelector("$.data.content").select(page.getRawText())));
			page.putField("summary", new JsonPathSelector("$.data.summary").select(page.getRawText())); // System.out.println("XXXXXXXX");
			page.putField("cover", new JsonPathSelector("$.data.cover").select(page.getRawText()));
			page.putField("published_at", new JsonPathSelector("$.data.published_at").select(page.getRawText()));
			// 因為數據是unicode【\\u6d88】格式的，需要转换看才能看到汉字
			page.putField("extraction_tags", StringEscapeUtils
					.unescapeJava(new JsonPathSelector("$.data.extraction_tags").select(page.getRawText())));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see us.codecraft.webmagic.processor.PageProcessor#getSite()
	 */
	@Override
	public Site getSite() {
		return site;// .setDomain("36kr.com").addStartUrl("http://36kr.com/api/post?column_id=&b_id=&per_page=20");
	}

	public static void main(String[] args) {
		// System.out.println(StringEscapeUtils.unescapeJava("[[\"\\u6d88\\u8d39\",1],[\"\\u6d88\\u8d39\\u5347\\u7ea7\",2]]"));
		//
		// System.out.println(StringEscapeUtils.unescapeHtml4("<p>&#x690D;&#x7269;&#x7F8E;&#x5B66;&#x5185;&#x5BB9;&#x5E73;&#x53F0;"));
		//
		// System.out.println(StringEscapeUtils.unescapeHtml3("<p>&#x690D;&#x7269;&#x7F8E;&#x5B66;&#x5185;&#x5BB9;&#x5E73;&#x53F0;"));
		//
		// System.out.println(StringEscapeUtils.unescapeXml("<p>&#x690D;&#x7269;&#x7F8E;&#x5B66;&#x5185;&#x5BB9;&#x5E73;&#x53F0;"));

		Spider.create(new Kr36Processor())
				.addUrl("http://36kr.com/api/post?column_id=&b_id=&per_page=20&_=" + System.currentTimeMillis()).run();
	}
}
