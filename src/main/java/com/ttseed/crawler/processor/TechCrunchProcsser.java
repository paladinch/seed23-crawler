/**
 *  处理TechCrunch 网站新闻
 *
 * Created by paladin 
 * Created at 2016年11月21日
 */
package com.ttseed.crawler.processor;

import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * @author paladin
 *
 */
public class TechCrunchProcsser implements PageProcessor {

	private Site site = Site.me();

	private static final String FISRT_URL = "http://techcrunch.cn/";

	private static final String LIST_URL = ".*/page/.*";

	private static final String SINGLE_URL = ".*/[0-9]{4}/[0-9]{2}/[0-9]{2}/.*";

	/*
	 * (non-Javadoc)
	 * 
	 * @see us.codecraft.webmagic.processor.PageProcessor#process(us.codecraft.
	 * webmagic.Page)
	 */
	@Override
	public void process(Page page) {
		if (page.getUrl().toString().equalsIgnoreCase(FISRT_URL)) {
			// 处理首页的数据
			List<String> requests = page.getHtml().links().regex(SINGLE_URL).all();
			page.addTargetRequests(requests);
			System.out.println("First page:" + requests);

			// 添加分页页面
			// for(int i = 2; i <= 5; i++){
			// page.addTargetRequest("http://techcrunch.cn/page/"+i);
			// }

		} else if (page.getUrl().regex(LIST_URL).match()) {
			List<String> requests = page.getHtml().links().regex(SINGLE_URL).all();
			// page.addTargetRequests(requests);
			System.out.println("First page:" + requests);
		} else if (page.getUrl().regex(SINGLE_URL).match()) {
			// 处理单页内容信息
			System.out.println("Single page");
			page.putField("title", page.getHtml().xpath("//header[@class='article-header']//h1/text()")); // 读取h1节点内的文字内容
			page.putField("publish_time", page.getHtml().xpath(
					"//header[@class='article-header']/div[@class='title-left']/div[@class='byline']/time/@datetime | //header[@class='article-header']/div[@class='byline']/time/@datetime"));
			page.putField("oriURL", page.getUrl());

			page.putField("topImg", page.getHtml()
					.xpath("//div[@class='article-entry']/img/@src | //article[@class='article']/img/@src")); // 读取img的src属性的值

			// 读该节点中所有p节点内容的方法. TODO:目前是list，需要拼接成字符串
			page.putField("content", page.getHtml().xpath("//div[@class='article-entry']/p").all()); // tidyText()读取旗下多个文字节点的内容，
																										// [position()>1]
		} else {
			System.out.println("UNProcess url:" + page.getUrl());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see us.codecraft.webmagic.processor.PageProcessor#getSite()
	 */
	@Override
	public Site getSite() {
		return site.setDomain("techcrunch.cn").addStartUrl(FISRT_URL);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Spider.create(new TechCrunchProcsser()).run();
	}

}
