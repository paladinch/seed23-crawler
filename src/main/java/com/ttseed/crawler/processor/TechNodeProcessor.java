/**
 * 处理动点科技(TechNode)新闻
 *  
 *
 * Created by paladin 
 * Created at 2016年11月21日
 */
package com.ttseed.crawler.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringEscapeUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.JsonPathSelector;
import us.codecraft.webmagic.utils.HttpConstant;

/**
 * @author paladin
 *
 */
public class TechNodeProcessor implements PageProcessor {

	// 可对site设置各种参数，

	// private Site site =
	// Site.me().setCycleRetryTimes(5).setRetryTimes(5).setSleepTime(500).setTimeOut(3
	// * 60 * 1000)
	// .setUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0)
	// Gecko/20100101 Firefox/38.0")
	// .addHeader("Accept",
	// "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	// .addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3")
	// .setCharset("UTF-8");

	private Site site = Site.me()
			.setUserAgent(
					"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36")
			.addHeader("Accept-Language", "zh-CN,zh;q=0.8").addHeader("Accept", "*/*")
			.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
			.addHeader("Accept-Encoding", "gzip, deflate")
			.addHeader("Referer", "http://cn.technode.com/")
			//.addHeader("X-Requested-With", "XMLHttpRequest")
			.addHeader("Origin", "http://cn.technode.com");

	// http://cn.technode.com/wp-admin/admin-ajax.php
	// https://www\\.huxiu\\.com/v2_action/article_list.*
	private static final String LIST_URL = "http://cn\\.technode\\.com/wp-admin/admin-ajax\\.php.*";
	private static final String FISRT_URL = "http://cn.technode.com";

	private static final String SINGLE_URL = ".*post/[0-9]{4}-[0-9]{2}-[0-9]{2}/.*";

	private static final String LIST_URL2 = ".*post/category/.*";

	private static AtomicInteger ai = new AtomicInteger(2);

	private String blockid = "";

	/*
	 * TODO: Ajax 读取分页信息目前出错。读不出数据。
	 * 
	 * @see us.codecraft.webmagic.processor.PageProcessor#process(us.codecraft.
	 * webmagic.Page)
	 */
	@Override
	public void process(Page page) {
		if (page.getUrl().toString().equalsIgnoreCase(FISRT_URL)) {
			// 处理首页的数据
			List<String> requests = page.getHtml().links().regex(".*post/[0-9]{4}-[0-9]{2}-[0-9]{2}/.*").all();
			page.addTargetRequests(requests);
			System.out.println("First page:" + requests);

			blockid = page.getHtml().xpath("//div[@class='td-load-more-wrap']//a/@data-td_block_id").toString();
			// 增加ajax渲染页面的url请求
			// ajax 传post参数，在webmagic中的使用方法。，
			// page.addTargetRequest("https://www.huxiu.com/v2_action/article_list");
			Request r = createNewListPage("http://cn.technode.com/wp-admin/admin-ajax.php", 2);

			System.out.println(r);

			//page.addTargetRequest(r);
		} else if (page.getUrl().regex(LIST_URL).match()) {
            //site.addHeader("X-Requested-With", "");
			System.out.println("Ajax List:" + page.getRawText());
			try {
				String data = StringEscapeUtils
						.unescapeJava(new JsonPathSelector("$.td_data").select(page.getRawText()));

				Html html = new Html(data);
				List<String> requests = html.links().regex(".*post/.*").all();
				// List<String> requests = new ArrayList<String>();
				//
				// for (String tt : requestst) {
				// if (StringUtils.isNotBlank(tt) && tt.startsWith("/article/"))
				// {
				// tt = "https://www.huxiu.com" + tt;
				// }
				//
				// requests.add(tt);
				// }
				System.out.println(requests);

				// page.addTargetRequests(requests);

				// 增加ajax渲染页面的url请求
				// TODO: ajax 传post参数，在webmagic中的使用方法。，需要定义循环读取全站信息。防止多线程冲突
				Request r = createNewListPage("http://cn.technode.com/wp-admin/admin-ajax.php?xx=" + UUID.randomUUID(),
						ai.incrementAndGet());
				System.out.println(r);
				page.addTargetRequest(r);
			} catch (Exception e) {
				page.setSkip(true);

				e.printStackTrace();
			}
		} else if (page.getUrl().regex(SINGLE_URL).match()) {
			// 处理单页内容信息
			System.out.println("Single page");
			page.putField("title", page.getHtml().xpath("//article[@class='type-post']//header//h1/text()")); // 读取h1节点内的文字内容
			page.putField("publish_time",
					page.getHtml().xpath("//article[@class='type-post']//header//div//time/@datetime"));
			page.putField("oriURL", page.getUrl());
			page.putField("topImg", page.getHtml().xpath("//article[@class='type-post']//p[1]//img/@src")); // 读取img的src属性的值

			//读该节点中所有p节点内容的方法. TODO:目前是list，需要拼接成字符串
			page.putField("content", page.getHtml().xpath("//article[@class='type-post']/p").all()); // tidyText()读取旗下多个文字节点的内容，
																									// [position()>1]

			// html()该节点内的所有html内容
		} else if (page.getUrl().regex(LIST_URL2).match()) {
			// 处理首页的数据
			List<String> requests = page.getHtml().links().regex(".*post/.*").all();

			System.out.println("List_URL2:" + requests);
			// page.addTargetRequests(requests);
		} else {
			System.out.println("UNProcess url:" + page.getUrl());
		}

	}

	/**
	 * 构造新的列表页面请求
	 * 
	 * @param url
	 * @param page
	 * @return
	 */
	private synchronized Request createNewListPage(String url, int page) {
		//site.addHeader("X-Requested-With", "XMLHttpRequest");
		
		Request r = new Request(url);
		r.setMethod(HttpConstant.Method.POST);
		
		Map<String, Object> extras = new HashMap<String, Object>();
		extras.put("action", "td_ajax_block");
		extras.put("td_atts",
				"{\"limit\":\"15\",\"custom_title\":\"%E6%9C%80%E6%96%B0%E6%96%87%E7%AB%A0\",\"ajax_pagination\":\"load_more\"}");
		extras.put("td_cur_cat", "");
		extras.put("td_block_id", blockid);
		extras.put("td_column_number", 3);
		extras.put("td_current_page", page);
		extras.put("block_type", 6);
		r.setExtras(extras);

		return r;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see us.codecraft.webmagic.processor.PageProcessor#getSite()
	 */
	@Override
	public Site getSite() {
		return site.setDomain("cn.technode.com").addStartUrl("http://cn.technode.com");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Spider.create(new TechNodeProcessor()).run();
	}

}
