package com.ttseed.crawler.processor;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * 
 * 天涯 抓取工具
 * 
 * @author paladin
 *
 */

public class TianyaProcessor implements PageProcessor {
    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);
    private static int MAX_CONTENT_LENGTH = 16000;

    public void process(Page page) {
        page.addTargetRequests(page.getHtml().links().regex("(http://bbs\\.tianya\\.cn/[a-zA-Z0-9_-]+\\.shtml)").all());
        page.putField("title", page.getHtml().xpath("//title").toString());
        String content = page.getHtml().smartContent().toString();
        page.putField("content", content.substring(0, Math.min(MAX_CONTENT_LENGTH, content.length())));
        page.putField("url", page.getUrl().toString());

        if (content.isEmpty()) {
            page.setSkip(true);
        }
    }

    public Site getSite() {
        return site;
    }

}
