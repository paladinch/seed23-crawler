/**
 *
 *
 * Created by paladin 
 * Created at 2016年11月10日
 */
package com.ttseed.crawler.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.JsonPathSelector;
import us.codecraft.webmagic.utils.HttpConstant;

/**
 * 虎嗅抓取处理
 * 
 * @author paladin
 *
 */
public class HuxiuProcessor implements PageProcessor {

	private static final String LIST_URL = "https://www\\.huxiu\\.com/v2_action/article_list.*";
	private static final String FISRT_URL = "http://www.huxiu.com";

	private static AtomicInteger ai = new AtomicInteger(1);

	// https://www.huxiu.com/v2_action/article_list
	// http://36kr\\.com/api/post\\?column_id=&b_id=.*
	// https://www\\.huxiu\\.com/v2_action/article_list\\?page=.*

	/*
	 * (non-Javadoc)
	 * 
	 * @see us.codecraft.webmagic.processor.PageProcessor#process(us.codecraft.
	 * webmagic.Page)
	 */
	@Override
	public void process(Page page) {
		if (page.getUrl().toString().equalsIgnoreCase(FISRT_URL)) {
			// 处理虎嗅首页的数据
			List<String> requests = page.getHtml().links().regex(".*article.*").all();
			page.addTargetRequests(requests);

			// 增加ajax渲染页面的url请求
			// ajax 传post参数，在webmagic中的使用方法。，
			// page.addTargetRequest("https://www.huxiu.com/v2_action/article_list");
			Request r = new Request("https://www.huxiu.com/v2_action/article_list");
			r.setMethod(HttpConstant.Method.POST);
			Map<String, Object> extras = new HashMap<String, Object>();
			extras.put("page", 1);
			extras.put("huxiu_hash_code", "9a6b6dfcb6d093af29bf4660623b340d");
			extras.put("last_dateline", System.currentTimeMillis() / 1000);
			r.setExtras(extras);

			page.addTargetRequest(r);
		} else if (page.getUrl().regex(LIST_URL).match()) {
			String data = StringEscapeUtils.unescapeJava(new JsonPathSelector("$.data").select(page.getRawText()));

			Html html = new Html(data);
			List<String> requestst = html.links().regex(".*article.*").all();
			List<String> requests = new ArrayList<String>();

			for (String tt : requestst) {
				if (StringUtils.isNotBlank(tt) && tt.startsWith("/article/")) {
					tt = "https://www.huxiu.com" + tt;
				}

				requests.add(tt);
			}
			//System.out.println(requests);

			// page.addTargetRequests(requests);

			// 增加ajax渲染页面的url请求
			// TODO: ajax 传post参数，在webmagic中的使用方法。，需要定义循环读取全站信息。防止多线程冲突
			Request r = new Request("https://www.huxiu.com/v2_action/article_list?xx="+ai.get());//加上无用的参数，防止webmagic去重 
			r.setMethod(HttpConstant.Method.POST);
			Map<String, Object> extras = new HashMap<String, Object>();
			extras.put("page", ai.incrementAndGet());
			extras.put("huxiu_hash_code", "9a6b6dfcb6d093af29bf4660623b340d");
			extras.put("last_dateline", System.currentTimeMillis() / 1000);
			r.setExtras(extras);
            //System.out.println(r);
			page.addTargetRequest(r);
		} else {
			// 处理单页内容信息
			System.out.println("Single page");
			page.putField("title", page.getHtml().xpath("//div[@class='article-wrap']//h1/text()")); // 读取h1节点内的文字内容
			page.putField("oriURL", page.getUrl());
			page.putField("topImg", page.getHtml().xpath("//div[@class='article-img-box']//img/@src")); // 读取img的src属性的值
			page.putField("content", page.getHtml().xpath("//div[@id='article_content']/tidyText()")); // tidyText()读取旗下多个文字节点的内容，
																										// html()该节点内的所有html内容
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see us.codecraft.webmagic.processor.PageProcessor#getSite()
	 */
	@SuppressWarnings("deprecation")
	@Override
	public Site getSite() {
		return Site.me().setDomain("www.huxiu.com").addStartUrl("http://www.huxiu.com");
	}

	public static void main(String[] args) {
		Spider.create(new HuxiuProcessor()).run();
	}
}
