/**
 *
 *
 * Created by paladin 
 * Created at 2016年11月10日
 */
package com.ttseed.crawler;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.ini4j.ConfigParser;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SchedulerMetaData;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author paladin
 *
 */
public class Runner {
	private static Logger log = LoggerFactory.getLogger(Runner.class);

	/**
	 * 运行各种job
	 * 
	 * @throws Exception
	 */
	public void run(ConfigParser configParser) throws Exception {

		// First we must get a reference to a scheduler
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();

		log.info("------- Initialization Complete --------");

		log.info("------- Scheduling Jobs ----------------");

		// jobs can be scheduled before sched.start() has been called

		// job 1 will run every 20 seconds
		Class forName;
		JobDetail job;
		CronTrigger trigger;
		Date ft;
		String jobclazz, cron;

		List<String> sections = configParser.sections();
		int jobcount = 0;
		for (String section : sections) {
			System.out.println("section:" + section);
			if (StringUtils.containsIgnoreCase(section, "job-")) {
				try {
					jobclazz = configParser.get(section, "jobclass");
					cron = configParser.get(section, "cron");
					forName = Class.forName(jobclazz);

					job = newJob(forName).withIdentity(section, "group1").build();

					trigger = newTrigger().withIdentity("trigger-" + section, "group1").withSchedule(cronSchedule(cron))
							.build();

					ft = sched.scheduleJob(job, trigger);
					log.info(job.getKey() + " has been scheduled to run at: " + ft + " and repeat based on expression: "
							+ trigger.getCronExpression());

					jobcount++;
				} catch (Exception e) {
                    log.error("定义"+section+"定时计划出错：",e);
				}
			}
		}
		
		log.info("===========defined "+jobcount + " kinds jobs=========");

		log.info("------- Starting Scheduler ----------------");

		// All of the jobs have been added to the scheduler, but none of the
		// jobs
		// will run until the scheduler has been started
		sched.start();

		log.info("------- Started Scheduler -----------------");

		log.info("------- Waiting five minutes... ------------");
		try {
			// wait five minutes to show jobs
			Thread.sleep(300L * 1000L);
			// executing...
		} catch (Exception e) {
			//
		}

		log.info("------- Shutting Down ---------------------");

		sched.shutdown(true);

		log.info("------- Shutdown Complete -----------------");

		SchedulerMetaData metaData = sched.getMetaData();
		log.info("Executed " + metaData.getNumberOfJobsExecuted() + " jobs.");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException, ConfigParser.NoSectionException,
			ConfigParser.NoOptionException, ConfigParser.InterpolationException, Exception {
		if (args.length != 1) {
			System.out.println("usage: java -jar SeedCrawler.jar config.ini");
		}

		ConfigParser configParser = new ConfigParser();
		configParser.read(new File(args[0]));

		// List<String> sections = configParser.sections();

		// for(String section : sections){
		// System.out.println("section:" + section);
		// }

		log.info("主程序启动！");
		// OtsConfig otsConfig = new OtsConfig(configParser.get("ots",
		// "endpoint"), configParser.get("ots", "accessid"),
		// configParser.get("ots", "accesskey"), configParser.get("ots",
		// "instance_name"), configParser.get("ots", "table_name"));
		// EsConfig esConfig = new EsConfig(configParser.get("elasticsearch",
		// "cluster_name"), configParser.get("elasticsearch", "host"),
		// configParser.getInt("elasticsearch", "java_port"),
		// configParser.get("elasticsearch", "index"),
		// configParser.get("elasticsearch", "type"));

		// int threadNum = configParser.getInt("spider", "thread_num");

		// Spider.create(new TianyaProcessor()).addUrl("http://bbs.tianya.cn/")
		// .setScheduler(new FileCacheQueueScheduler("tianyaUrlCache"))
		// //.addPipeline(new OtsPipeline(otsConfig, esConfig))
		// .thread(threadNum).run();

		Runner runner = new Runner();
		runner.run(configParser);
	}

}
