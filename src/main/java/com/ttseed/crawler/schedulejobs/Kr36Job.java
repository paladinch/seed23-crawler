/**
 *
 *
 * Created by paladin 
 * Created at 2016年11月24日
 */
package com.ttseed.crawler.schedulejobs;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author paladin
 *
 */
public class Kr36Job implements Job {
	private static Logger log = LoggerFactory.getLogger(Kr36Job.class);
	/**
     * Quartz requires a public empty constructor so that the
     * scheduler can instantiate the class whenever it needs.
     */
	public Kr36Job(){
		
	}
	
	/**
     * <p>
     * Called by the <code>{@link org.quartz.Scheduler}</code> when a
     * <code>{@link org.quartz.Trigger}</code> fires that is associated with
     * the <code>Job</code>.
     * </p>
     * 
     * @throws JobExecutionException
     *             if there is an exception while executing the job.
     */
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// This job simply prints out its job name and the
        // date and time that it is running
        JobKey jobKey = context.getJobDetail().getKey();
        log.info("Kr36Job says: " + jobKey + " executing at " + new Date());
	}

}
